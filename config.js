var rc = require('rc'),
  defaultConfig = {
    title: 'Dillinger Markdown Editor',
    description: 'Dillinger is an online cloud based HTML5 filled Markdown Editor. ' +
      'Convert HTML to Markdown. 100% Open Source!',
    keywords: 'Markdown, Dillinger, Editor, ACE, Github, Open Source, Node.js',
    author: 'Joe McCann and Martin Broder'
  };

module.exports = function() {
  return rc('dillinger', defaultConfig);
};
