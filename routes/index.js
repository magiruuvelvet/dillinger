'use strict'

var path = require('path')
  , request = require('request')
  , qs = require('querystring')
  ;

// Show the home page
exports.index = function(req, res) {
    return res.render('index')
}

// Show the not implemented yet page
exports.not_implemented = function(req, res) {
  res.render('not-implemented')
}
